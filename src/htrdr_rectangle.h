/* Copyright (C) 2018-2019 CNRS, |Meso|Star>, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTRDR_RECTANGLE_H
#define HTRDR_RECTANGLE_H

#include <rsys/rsys.h>

struct htrdr;
struct htrdr_rectangle; /* 2D rectangle transformed in 3D */

extern LOCAL_SYM res_T
htrdr_rectangle_create
  (struct htrdr* htrdr,
   const double sz[2], /* Size of the rectangle along its local X and Y axis */
   const double pos[3], /* World space position of the rectangle center */
   const double tgt[3], /* Vector orthognal to the rectangle plane */
   const double up[2], /* vector orthogonal to the rectangle X axis */
   struct htrdr_rectangle** rect);

extern LOCAL_SYM void
htrdr_rectangle_ref_get
  (struct htrdr_rectangle* rect);

extern LOCAL_SYM void
htrdr_rectangle_ref_put
  (struct htrdr_rectangle* rect);

extern LOCAL_SYM void
htrdr_rectangle_sample_pos
  (const struct htrdr_rectangle* rect,
   const double sample[2], /* In [0, 1[ */
   double pos[3]);

#endif /* HTRDR_RECTANGLE_H */

